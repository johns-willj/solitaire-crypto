module Solitaire.Deck (Suit, Rank, Card, Deck) where

data Suit = Club | Diamond | Heart | Spade
    deriving (Show, Ord, Eq, Enum)

data Rank =
    Ace | Two | Three | Four | Five
    | Six | Seven | Eight | Nine | Ten
    | Jack | Queen | King
    deriving (Show, Ord, Eq, Enum)

data Card = JokerA | JokerB | Card Suit Rank
    deriving (Show)

instance Enum Card where
    fromEnum JokerA     = 53
    fromEnum JokerB     = 53
    fromEnum (Card s r) = (fromEnum s) * 13 + (fromEnum r + 1)

    toEnum i = let
        suit = toEnum ((i - 1) `div` 13)
        rank = toEnum ((i - 1) `rem` 13)
        in Card suit rank

type Deck = [Card]
