module Solitaire.Cypher (encipher, decipher) where

import Data.Char (ord, chr, toUpper)

alphaShift :: Int
alphaShift = 64

newtype Char' = Char' Char
instance Enum Char' where
    fromEnum c
        | upperShiftedOrd < 1   = invalidCharacterError
        | upperShiftedOrd > 26  = invalidCharacterError
        | otherwise             = upperShiftedOrd
        where
            upperShiftedOrd = (ord $ toUpper c) - alphaShift
            invalidCharacterError = error "character must be a lower- or upper-case letter"

    toEnum i
        | i < 1     = invalidNumberError
        | i > 26    = invalidNumberError
        | otherwise = chr (i + alphaShift)
        where
            invalidNumberError = error "value must be between 1 and 26, inclusive" 

